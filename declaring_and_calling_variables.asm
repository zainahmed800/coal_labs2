
.model small
.stack 100h

.data
var1 db 0ah,0dh, "zain $"
var2 db 0ah,0dh, "1 $"

.code
main proc
    
mov ax,@data
mov ds,ax
mov dl, offset var1
mov ah,09
int 21h

mov ax,@data
mov ds,ax
mov dl, offset var2
mov ah,09
int 21h

main endp
end main



